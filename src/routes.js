import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SplashScreen from './screens/splashscreen';
import Intro from './screens/intro';
import Login from './screens/Login';
import Profile from './screens/profile';
import Register from './screens/register';
import Home from './screens/home';
import Maps from './screens/Maps';
import Icon from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';
import ReactNativeScreen from './screens/ReactNative';
import Chat from './screens/Chat';
import Asyncstorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabScreen = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = 'home';
          return <Icon name={iconName} size={size} color={color} />;
        } else if (route.name === 'Maps') {
          iconName = 'location';
          return <Entypo name={iconName} size={size} color={color} />;
        } else if (route.name === 'Chat') {
          iconName = 'chat';
          return <Entypo name={iconName} size={size} color={color} />;
        } else {
          iconName = 'person';
          return <Icon name={iconName} size={size} color={color} />;
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: '#088dc4',
      inactiveTintColor: 'gray',
    }}>
    <Tab.Screen name="Home" children={HomeNavigation} />
    <Tab.Screen name="Maps" component={Maps} />
    <Tab.Screen name="Chat" component={Chat} />
    <Tab.Screen name="Profile" component={Profile} />
  </Tab.Navigator>
);

const MainNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Intro"
      component={Intro}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Login"
      component={Login}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="HomeTab"
      children={TabScreen}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="Register"
      component={Register}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

// const MainNavigation2 = () => (
//   <Stack.Navigator>
//     <Stack.Screen
//       name="Login"
//       component={Login}
//       options={{headerShown: false}}
//     />
//     <Stack.Screen
//       name="HomeTab"
//       children={TabScreen}
//       options={{headerShown: false}}
//     />
//     <Stack.Screen
//       name="Register"
//       component={Register}
//       options={{headerShown: false}}
//     />
//   </Stack.Navigator>
// );

const HomeNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="HomeScreen"
      component={Home}
      options={{headerShown: false}}
    />
    <Stack.Screen
      name="ReactNative"
      component={ReactNativeScreen}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

function AppNavigation({navigation}) {
  const [isLoading, setIsLoading] = React.useState(true);
  // const [isInstalled, setIsInstalled] = React.useState('no');

  //mengatur durasi splashscreen saat aplikasi pertama kali dibuka
  React.useEffect(() => {
    // async function isInstalled() {
    //   const isInstalled = await Asyncstorage.getItem('isInstalled');
    //   setIsInstalled(isInstalled);
    //   // console.log('isInstalled -> installStatus', isInstalled);
    // }
    // isInstalled();
    setTimeout(() => {
      setIsLoading(!isLoading);
    }, 2000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  // if (isInstalled === 'yes') {
  //   console.log(isInstalled);
  //   return (
  //     <NavigationContainer>
  //       <MainNavigation2 />
  //     </NavigationContainer>
  //   );
  // }
  // else if (isInstalled === 'no') {
  return (
    <NavigationContainer>
      <MainNavigation />
      {/* <TabScreen /> */}
    </NavigationContainer>
  );
  // }
}

export default AppNavigation;
