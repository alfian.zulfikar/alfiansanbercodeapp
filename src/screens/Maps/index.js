import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1IjoiYWxmaWFuenVsZmlrYXIiLCJhIjoiY2tldGN5MmN5MWsweDJ5cnd4OXpyYmkydyJ9.mrEPXtRexjaZqq3Fav6svg',
);

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

export default function Maps() {
  const [point, setPoint] = useState(coordinates);

  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };
    getLocation();
  }, []);

  const renderAnnotation = (counter) => {
    // console.log(counter);
    const id = `pointAnnotation${counter}`;
    const coordinate = point[counter];
    const title = `Longitude: ${point[counter][0]} Latitude: ${point[counter][1]}`;

    return (
      <MapboxGL.PointAnnotation
        key={id}
        id={id}
        title="Test"
        coordinate={coordinate}>
        <MapboxGL.Callout title={title} />
      </MapboxGL.PointAnnotation>
    );
  };

  const renderAnnotations = () => {
    const items = [];

    for (let i = 0; i < point.length; i++) {
      items.push(renderAnnotation(i));
    }

    return items;
  };

  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {renderAnnotations()}
      </MapboxGL.MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
