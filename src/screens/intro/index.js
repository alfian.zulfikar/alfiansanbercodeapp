import React, {useEffect} from 'react';
import {View, Text, Image, StatusBar, StyleSheet} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import Asyncstorage from '@react-native-community/async-storage';

//data yang akan digunakan dalam onboarding
const slides = [
  {
    key: 1,
    title: 'Belajar Intensif',
    text:
      '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: require('../../assets/images/working-time.png'),
  },
  {
    key: 2,
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: require('../../assets/images/research.png'),
  },
  {
    key: 3,
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: require('../../assets/images/venture.png'),
  },
  {
    key: 4,
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: require('../../assets/images/money-bag.png'),
  },
];

const Intro = ({navigation}) => {
  useEffect(() => {
    async function isInstalled() {
      const isInstalled = await Asyncstorage.getItem('isInstalled');
      // console.log('isInstalled -> installStatus', isInstalled);
      if (isInstalled === 'yes') {
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      }
    }
    isInstalled();
  }, []);

  const setIsInstalled = async () => {
    async function isInstalled() {
      await Asyncstorage.setItem('isInstalled', 'yes');
      // console.log('isLogin -> loginStatus', loginStatus);
    }
    isInstalled();
  };
  //menampilkan data slides kedalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
  const onDone = () => {
    // navigation.navigate('Login');
    setIsInstalled();
    navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };

  //mengcustom tampilan button done
  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  //mengcustom tampilan next button
  const renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <AppIntroSlider
        data={slides}
        onDone={onDone}
        renderItem={renderItem}
        renderDoneButton={renderDoneButton}
        renderNextButton={renderNextButton}
        keyExtractor={(item, index) => index.toString()}
        activeDotStyle={{backgroundColor: '#191970'}}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    marginHorizontal: 20,
  },
  title: {
    color: '#191970',
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 100,
  },
  image: {
    marginVertical: 30,
  },
  text: {
    color: '#a4a4a6',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  buttonCircle: {
    backgroundColor: '#191970',
    width: 40,
    height: 40,
    backgroundColor: '#191970',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Intro;
