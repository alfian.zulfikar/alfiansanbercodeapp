import React, {useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function home({navigation}) {
  // useEffect(() => {
  //   async function isLogin() {
  //     const isLogin = await Asyncstorage.getItem('isLogin');
  //     // console.log('isLogin -> loginStatus', loginStatus);
  //     if(isLogin === 'yes') {

  //     }
  //   }
  //   isLogin();
  // }, []);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* kelas */}
        <View style={styles.kelasContainer}>
          <View style={styles.header}>
            <Text style={styles.headerText}>Kelas</Text>
          </View>
          <View style={styles.kelasItem}>
            <TouchableOpacity
              style={styles.item}
              onPress={() => navigation.navigate('ReactNative')}>
              <Icon name="logo-react" size={50} color="#fff" />
              <Text style={styles.itemText}>React Native</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <Icon name="logo-python" size={50} color="#fff" />
              <Text style={styles.itemText}>Data Science</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <Icon name="logo-react" size={50} color="#fff" />
              <Text style={styles.itemText}>React JS</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <Icon name="logo-laravel" size={50} color="#fff" />
              <Text style={styles.itemText}>Laravel</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* kelas */}
        <View style={styles.kelasContainer}>
          <View style={styles.header}>
            <Text style={styles.headerText}>Kelas</Text>
          </View>
          <View style={styles.kelasItem}>
            <TouchableOpacity style={styles.item}>
              <Icon name="logo-wordpress" size={50} color="#fff" />
              <Text style={styles.itemText}>Wordpress</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <Image
                source={require('../../assets/images/website-design.png')}
                style={{width: 50, height: 50}}
              />
              <Text style={styles.itemText}>Desain Grafis</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <MaterialCommunityIcons name="server" size={50} color="#fff" />
              <Text style={styles.itemText}>Web Server</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item}>
              <Image
                source={require('../../assets/images/ux.png')}
                style={{width: 50, height: 50}}
              />
              <Text style={styles.itemText}>UI/UX Design</Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* Summary */}
        <View style={styles.summaryContainer}>
          <View style={styles.header}>
            <Text style={styles.headerText}>Summary</Text>
          </View>
          <View style={styles.itemSummary}>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>React Native</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>20 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>20 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>Data Science</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>React JS</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>66 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>Laravel</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>60 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>Wordpress</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>60 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>Design Grafis</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>60 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>Web Server</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>60 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
            <View style={styles.itemDetailContainer}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemTitleText}>UI/UX Design</Text>
              </View>
              <View style={styles.itemDetail}>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Today</Text>
                  <Text style={styles.detailText}>60 orang</Text>
                </View>
                <View style={styles.detailContainer}>
                  <Text style={styles.detailText}>Total</Text>
                  <Text style={styles.detailText}>100 orang</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    paddingBottom: 0,
  },
  kelasContainer: {
    width: '100%',
    backgroundColor: '#3EC6FF',
    borderRadius: 10,
    marginBottom: 10,
  },
  header: {
    backgroundColor: '#088dc4',
    borderTopStartRadius: 10,
    borderTopEndRadius: 10,
    padding: 7,
  },
  headerText: {
    color: '#fff',
    fontSize: 16,
  },
  kelasItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 7,
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemText: {
    color: '#fff',
  },
  summaryContainer: {
    backgroundColor: '#088dc4',
  },
  itemTitle: {
    backgroundColor: '#3EC6FF',
    padding: 7,
  },
  itemTitleText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  itemDetail: {
    padding: 7,
  },
  detailContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: '#088dc4',
  },
  detailText: {
    color: '#fff',
  },
});
