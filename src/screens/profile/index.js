import React, {useState, useEffect} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import {GoogleSignin} from '@react-native-community/google-signin';
import AsyncStorage from '@react-native-community/async-storage';

export default function profil({navigation}) {
  const [userInfo, setUserInfo] = useState(null);
  const [method, setMethod] = useState('');
  const [imageData, setImageData] = useState('');
  const [nameData, setNameData] = useState('');
  const [emailData, setEmailData] = useState('');

  useEffect(() => {
    async function getToken() {
      try {
        const token = await Asyncstorage.getItem('token');
        const methodPass = await AsyncStorage.getItem('method');
        setMethod(methodPass);
        dataSourceOptions(methodPass);
        // console.log('getToken -> token', token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    const userInfo = await GoogleSignin.signInSilently();
    // console.log('getCurrentUser -> userInfo', userInfo);
    setUserInfo(userInfo);
  };

  // const getVenue = (token) => {
  //   Axios.get(`${api}/venues`, {
  //     timeout: 20000,
  //     headers: {
  //       Authorization: 'Bearer' + token,
  //     },
  //   })
  //     .then((res) => {
  //       console.log('Profile -> res', res);
  //     })
  //     .catch((err) => {
  //       console.log('Profile -> err', err);
  //     });
  // };

  const onLogoutPress = async () => {
    try {
      if (method == 'google') {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
        await Asyncstorage.removeItem('isLogin');
        // navigation.navigate('Login');
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      } else if (method == 'jwt') {
        await Asyncstorage.removeItem('token');
        await Asyncstorage.removeItem('isLogin');
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      } else {
        await Asyncstorage.removeItem('isLogin');
        navigation.reset({
          index: 0,
          routes: [{name: 'Login'}],
        });
      }
    } catch (err) {
      console.log('profile -> err', err);
    }
  };

  const dataSourceOptions = (method) => {
    if (method === 'jwt') {
      setImageData(require('../../assets/images/profile.jpeg'));
      setNameData('Alfian Zulfikar');
      setEmailData('alfian.zulfikar@gmail.com');
    } else if (method === 'google') {
      setImageData({uri: userInfo && userInfo.user && userInfo.user.photo});
      setNameData(userInfo && userInfo.user && userInfo.user.name);
      setEmailData(userInfo && userInfo.user && userInfo.user.email);
    } else {
      setImageData(require('../../assets/images/profile.jpeg'));
      setNameData('Alfian Zulfikar');
      setEmailData('alfian.zulfikar@gmail.com');
    }
  };

  return (
    <View style={styles.container}>
      {/* background */}
      <View style={styles.topArea}></View>
      <View style={styles.bottomArea}></View>

      {/* contain */}
      <View style={styles.contain}>
        <Image source={imageData} style={styles.profileImage} />
        <Text style={styles.name}>{nameData}</Text>
        <View style={styles.dataArea}>
          <View style={styles.dataItem}>
            <Text>Tanggal lahir</Text>
            <Text>8 November 1997</Text>
          </View>
          <View style={styles.dataItem}>
            <Text>Jenis kelamin</Text>
            <Text>Laki-laki</Text>
          </View>
          <View style={styles.dataItem}>
            <Text>Hobi</Text>
            <Text>Ngoding dan Desain</Text>
          </View>
          <View style={styles.dataItem}>
            <Text>No. Telp</Text>
            <Text>087742351592</Text>
          </View>
          <View style={styles.dataItem}>
            <Text>Email</Text>
            <Text>{emailData}</Text>
          </View>
          <TouchableOpacity
            style={styles.button}
            onPress={() => onLogoutPress()}>
            <Text style={styles.buttonText}>LOGOUT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topArea: {
    flex: 2,
    width: '100%',
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
  },
  bottomArea: {
    flex: 3,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contain: {
    width: '90%',
    position: 'absolute',
    top: 35,
    alignItems: 'center',
  },
  profileImage: {
    height: 90,
    width: 90,
    borderRadius: 50,
  },
  name: {
    marginTop: 22,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 17.5,
  },
  dataArea: {
    marginTop: 22,
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  dataItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  button: {
    padding: 10,
    backgroundColor: '#3EC6FF',
    marginTop: 10,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
  },
});
