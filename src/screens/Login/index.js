import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Button,
} from 'react-native';
import Axios from 'axios';
import Asyncstorage from '@react-native-community/async-storage';
import api from '../../api';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

function Login({navigation}) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    async function isLogin() {
      configureGoogleSignin();
      const isLogin = await Asyncstorage.getItem('isLogin');
      // console.log('isLogin -> loginStatus', loginStatus);
      if (isLogin === 'yes') {
        navigation.reset({
          index: 0,
          routes: [{name: 'HomeTab'}],
        });
      }
    }
    isLogin();
  }, []);

  // const saveToken = async (token) => {
  //   try {
  //     await Asyncstorage.setItem('token', token);
  //     await Asyncstorage.setItem('method', 'jwt');
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  const fingerprintMethod = async () => {
    await Asyncstorage.setItem('method', 'fingerprint');
    await Asyncstorage.setItem('isLogin', 'yes');
  };

  const jwtMethod = async () => {
    await Asyncstorage.setItem('method', 'jwt');
    await Asyncstorage.setItem('isLogin', 'yes');
  };

  const configureGoogleSignin = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '572905709279-bmu8olpa8msc80ijagfrib8qr7rho0ag.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('signInWithGoogle -> idToken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);

      await Asyncstorage.setItem('method', 'google');
      await Asyncstorage.setItem('isLogin', 'yes');
      navigation.reset({
        index: 0,
        routes: [{name: 'HomeTab'}],
      });
    } catch (error) {
      console.log('signInWithGoogle -> error', error);
    }
  };

  const onLoginPress = () => {
    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        jwtMethod();
        navigation.reset({
          index: 0,
          routes: [{name: 'HomeTab'}],
        });
      })
      .catch((err) => {
        console.log('onLoginPress -> err', err);
      });
  };
  // const onLoginPress = () => {
  //   let data = {
  //     email: email,
  //     password: password,
  //   };

  //   Axios.post(`${api}/login`, data, {
  //     timeout: 20000,
  //   })
  //     .then((res) => {
  //       saveToken(res.data.token);
  //       navigation.navigate('HomeTab');
  //       console.log('onLoginPress -> token', res.data.token);
  //     })
  //     .catch((err) => {
  //       console.log('Login -> err', err);
  //     });
  // };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        fingerprintMethod();
        navigation.reset({
          index: 0,
          routes: [{name: 'HomeTab'}],
        });
      })
      .catch((error) => {
        alert('Authentication Failed');
      });
  };

  const register = () => {
    navigation.navigate('Register');
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.content}>
          <View style={styles.logoContainer}>
            <Image source={require('../../assets/images/logo.jpg')} />
          </View>
          <View style={styles.formContainer}>
            <View style={styles.formArea}>
              <View style={styles.formItem}>
                <Text style={styles.formText}>Username</Text>
                <TextInput
                  value={email}
                  placeholder="Username or Email"
                  onChangeText={(email) => setEmail(email)}
                  underlineColorAndroid="#c6c6c6"
                />
              </View>
              <View style={styles.formItem}>
                <Text style={styles.formText}>Password</Text>
                <TextInput
                  value={password}
                  placeholder="Password"
                  onChangeText={(password) => setPassword(password)}
                  underlineColorAndroid="#c6c6c6"
                  secureTextEntry={true}
                />
              </View>
              <View style={{marginTop: 10}}>
                <Button
                  onPress={() => onLoginPress()}
                  title="LOGIN"
                  color="#3EC6FF"
                />
              </View>
              <View style={styles.lineContainer}>
                <View style={styles.line}></View>
                <Text style={{marginHorizontal: 10}}>OR</Text>
                <View style={styles.line}></View>
              </View>
              <View style={{marginTop: 10}}>
                <GoogleSigninButton
                  onPress={() => signInWithGoogle()}
                  style={{width: '100%', height: 40}}
                  size={GoogleSigninButton.Size.Wide}
                  color={GoogleSigninButton.Color.Dark}
                />
              </View>
              <View style={{marginTop: 10}}>
                <Button
                  color="#191970"
                  title="SIGN IN WITH FINGERPRINT"
                  onPress={() => signInWithFingerprint()}
                />
              </View>
            </View>
            <View style={styles.Ask}>
              <Text>Belum mempunyai akun? </Text>
              <TouchableOpacity onPress={() => register()}>
                <Text>Buat Akun</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    width: '100%',
    alignItems: 'center',
  },
  formArea: {
    width: '100%',
  },
  formItem: {
    marginTop: 10,
  },
  formText: {},
  button: {
    padding: 10,
    backgroundColor: '#3EC6FF',
    marginTop: 10,
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff',
  },
  lineContainer: {
    marginTop: 10,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  line: {
    height: 2,
    borderColor: '#c6c6c6',
    borderTopWidth: 2,
    flex: 1,
  },
  Ask: {marginTop: 100, flexDirection: 'row'},
});

export default Login;
