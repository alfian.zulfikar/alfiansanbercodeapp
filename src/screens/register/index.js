import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  TextInput,
  Button,
  Modal,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import storage, {firebase} from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';

export default function Register() {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  useEffect(() => {
    auth().signInAnonymously();
  }, []);

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(options);
      // console.log('takePicture -> data', data);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadPhoto = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`image/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Seccess');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const renderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toogleCamera()}>
                <MaterialCommunity name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.shapeContainer}>
              <View style={styles.round} />
              <View style={styles.rectangle} />
            </View>
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                onPress={() => takePicture()}
                style={styles.btnTake}>
                <Icon name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.topArea} />
      <View style={styles.bottomArea} />
      <View style={styles.contain}>
        <Image
          source={
            photo === null
              ? require('../../assets/images/profile.jpeg')
              : {uri: photo.uri}
          }
          style={styles.profileImage}
        />
        <TouchableOpacity
          onPress={() => setIsVisible(true)}
          style={styles.btnChangeImage}>
          <Text style={styles.name}>Change Image</Text>
        </TouchableOpacity>
        {renderCamera()}
        <View style={styles.formArea}>
          <View style={styles.formItem}>
            <Text style={{fontWeight: 'bold'}}>Nama</Text>
            <TextInput placeholder="Nama" underlineColorAndroid="#c6c6c6" />
          </View>
          <View style={styles.formItem}>
            <Text style={{fontWeight: 'bold'}}>Email</Text>
            <TextInput placeholder="Email" underlineColorAndroid="#c6c6c6" />
          </View>
          <View style={styles.formItem}>
            <Text style={{fontWeight: 'bold'}}>Password</Text>
            <TextInput placeholder="Password" underlineColorAndroid="#c6c6c6" />
          </View>
          <View>
            <Button
              title="REGISTER"
              color="#3EC6FF"
              onPress={() => uploadPhoto(photo.uri)}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topArea: {
    flex: 1,
    width: '100%',
    backgroundColor: '#3EC6FF',
    alignItems: 'center',
  },
  bottomArea: {
    flex: 2,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contain: {
    width: '90%',
    position: 'absolute',
    top: 35,
    alignItems: 'center',
  },
  profileImage: {
    height: 90,
    width: 90,
    borderRadius: 50,
  },
  btnChangeImage: {
    marginTop: 22,
    alignItems: 'center',
  },
  name: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
  formArea: {
    marginTop: 22,
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    padding: 15,
  },
  formItem: {
    marginBottom: 10,
  },
  btnFlipContainer: {
    padding: 20,
  },
  btnFlip: {
    backgroundColor: '#fff',
    borderRadius: 50,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  shapeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  round: {
    height: 300,
    width: 200,
    borderRadius: 100,
    borderColor: '#fff',
    borderWidth: 3,
    left: 0,
    right: 0,
  },
  rectangle: {
    width: 200,
    height: 120,
    borderColor: '#fff',
    borderWidth: 3,
  },
  btnTakeContainer: {
    alignItems: 'center',
    marginTop: 40,
    paddingBottom: 20,
  },
  btnTake: {
    backgroundColor: '#fff',
    borderRadius: 50,
    height: 60,
    width: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
