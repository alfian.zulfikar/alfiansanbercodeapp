/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import AppNavigation from './src/routes';
import firebase from '@react-native-firebase/app';

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyD_fTq04zbiUBa7HGI5lQ5hPDs6nutdLrA',
  authDomain: 'sanbercode-19033.firebaseapp.com',
  databaseURL: 'https://sanbercode-19033.firebaseio.com',
  projectId: 'sanbercode-19033',
  storageBucket: 'sanbercode-19033.appspot.com',
  messagingSenderId: '572905709279',
  appId: '1:572905709279:web:581a39c4f7e0dc41e1d36a',
  measurementId: 'G-61CSP690PL',
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <AppNavigation />;
};

export default App;
